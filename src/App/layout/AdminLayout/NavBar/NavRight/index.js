import React, { Component } from "react";
import { Dropdown } from "react-bootstrap";

import Aux from "../../../../../hoc/_Aux";
import DEMO from "../../../../../store/constant";
import { Auth } from "aws-amplify";

import Avatar1 from "../../../../../assets/images/user/avatar-1.jpg";
import Avatar2 from "../../../../../assets/images/user/avatar-2.jpg";
import Avatar3 from "../../../../../assets/images/user/avatar-3.jpg";

class NavRight extends Component {
  constructor() {
    super();
  }
  state = {
    listOpen: false,
  };
  async signOut() {
    try {
      await Auth.signOut();
      localStorage.clear();
    } catch (error) {
      console.log("error signing out: ", error);
    }
  }

  render() {
    return (
      <Aux>
        <ul className="navbar-nav ml-auto">
          <li>
            <Dropdown className="drp-user">
              <Dropdown.Toggle as="a" variant="link" id="dropdown-basic">
                <span style={{cursor:'pointer',background:'#4886ff',color:'white',padding:'10px',borderRadius:'100%',fontSize:'17px'}}>S</span>
              </Dropdown.Toggle>
              <Dropdown.Menu alignRight className="profile-notification">
                <div className="pro-head">
                  <span style={{color:'white',fontSize:'17px'}}>Samyutha</span>
                </div>
                <ul className="pro-body">
                  <li>
                    <a href={DEMO.BLANK_LINK} className="dropdown-item">
                      <i className="feather icon-settings" /> Settings
                    </a>
                  </li>
                  <li>
                    <a href={"/users/profile/"} className="dropdown-item">
                      <i className="feather icon-user" /> Profile
                    </a>
                  </li>          
                  <li>
                    <a href={"/signin"} onClick={this.signOut} className="dud-logout" title="Signout" className="dropdown-item">
                      <i className="feather icon-log-out" /> Signout
                    </a>
                  </li>
                </ul>
              </Dropdown.Menu>
            </Dropdown>
          </li>
        </ul>
      </Aux>
    );
  }
}

export default NavRight;
