   import React, { useState } from "react";
   import { Row, Col, Card, Form, Button, Alert } from "react-bootstrap";
   import Aux from "../../hoc/_Aux";
   import Breadcrumb from "../../App/components/Breadcrumb";
   import { useForm } from "react-hook-form";
   import axios from 'axios';
   import "../../App.css"; 
   const AddGenres = () => {
     const { register, handleSubmit, errors } = useForm();
     const [showErr, setShowErr] = useState(false);
     const [showMsg, setShowMsg] = useState(false);
     const [errorMessage, setErrorMessage] = useState("");
     async function addGenre(data,e)
     {
         try
         {
            const url = 'http://3.6.84.135:8002/create/genre';
            const result = await axios.post(url, data);
            console.log(result);
         }
         catch(err)
         {
             console.log("error",err);
         }
     }
     return (   
       <Aux>
         <Row className="align-items-center page-header">
           <Col>
             <Breadcrumb />
           </Col>
         </Row>
         <Row>
           <Col>
             <Card>
               <Card.Header as="h3">
                 Add Genre
               </Card.Header>
               <Card.Body>
                 <hr />
                 <Form onSubmit={handleSubmit(addGenre)}>
                   <Row>
                     <Col md={8}></Col>
                     <Col md={4}>
                       <strong>Required fields are marked with</strong>
                       {""}
                       <span className="required"></span>
                     </Col>
                     <Col md={6}>
                       <Form.Group controlId="exampleForm.ControlInput1">
                         <Form.Label className="required">Genre Name</Form.Label>
                         <Form.Control
                           type="text"
                           name="name"
                           placeholder="Enter Genre Name"
                           ref={register({
                             required: {
                               value: true,
                               message: "Genre Name required",
                             }
                           })}
                         />
                         {errors.name && (
                           <p className="text-danger">
                             {errors.name.message}
                           </p>
                         )}
                       </Form.Group>
                   </Col>
                 </Row>
                   <Button variant="primary" type="submit">
                     Add
                   </Button>{" "}
                   <Button variant="outline-secondary" type="reset">
                     Clear
                   </Button>
                 </Form>
                 <br />
                 <Row>
                   <Col md={3} />
                   <Col md={6}>
                     {showMsg ? (
                       <Alert
                         variant="success"
                         onClose={() => setShowMsg(false)}
                         dismissible
                       >
                         Genre added successfully!
                       </Alert>
                     ) : (
                       ""
                     )}
   
                     {showErr ? (
                       <Alert
                         variant="danger"
                         onClose={() => setShowErr(false)}
                         dismissible
                       >
                         {errorMessage}
                       </Alert>
                     ) : (
                       ""
                     )}
                   </Col>
                 </Row>
               </Card.Body>
             </Card>
           </Col>
         </Row>
       </Aux>
     );
   };
   
   export default AddGenres;
   