import React, { useState } from "react";
import { Row, Col, Card, Form, Button, Alert } from "react-bootstrap";
import Aux from "../../hoc/_Aux";
import Breadcrumb from "../../App/components/Breadcrumb";
import { useForm } from "react-hook-form";
import axios from 'axios';
import "../../App.css"; 
const AddVideo = () => {
  const { register, handleSubmit, errors } = useForm();
  const [showErr, setShowErr] = useState(false);
  const [showMsg, setShowMsg] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  async function addVideo(data,e)
  {
      try
      {
         const url = 'http://3.6.84.135:8002/create/video';
         const result = await axios.post(url, data);
         console.log(result);
      }
      catch(err)
      {
          console.log("error",err);
      }
  }
  return (   
    <Aux>
      <Row className="align-items-center page-header">
        <Col>
          <Breadcrumb />
        </Col>
      </Row>
      <Row>
        <Col>
          <Card>
            <Card.Header as="h3">
              Add Video
            </Card.Header>
            <Card.Body>
              <hr />
              <Form onSubmit={handleSubmit(addVideo)}>
                <Row>
                  <Col md={8}></Col>
                  <Col md={4}>
                    <strong>Required fields are marked with</strong>
                    {""}
                    <span className="required"></span>
                  </Col>
                  <Col md={6}>
                    <Form.Group controlId="exampleForm.ControlInput1">
                      <Form.Label className="required">Title</Form.Label>
                      <Form.Control
                        type="text"
                        name="title"
                        placeholder="Enter Title"
                        ref={register({
                          required: {
                            value: true,
                            message: "Title required",
                          }
                        })}
                      />
                      {errors.title && (
                        <p className="text-danger">
                          {errors.title.message}
                        </p>
                      )}
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput1">
                      <Form.Label className="required">Age</Form.Label>
                      <Form.Control
                        type="number"
                        name="age"
                        placeholder="Enter Age"
                        ref={register({
                          required: {
                            value: true,
                            message: "Age required",
                          }
                        })}
                      />
                      {errors.age && (
                        <p className="text-danger">
                          {errors.age.message}
                        </p>
                      )}
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlSelect1">
                      <Form.Label className="required">Genres</Form.Label>
                      <Form.Control
                        as="select"
                        name="gernes"
                        defaultValue=""
                        ref={register({
                          required: {
                            value: true,
                            message: "Genre Required!",
                          },
                        })}
                      >
                        <option value="" disabled>
                          Select Genres
                        </option>
                      </Form.Control>
                      {errors.gernes && (
                        <p className="text-danger">
                          {errors.gernes.message}
                        </p>
                      )}
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlSelect1">
                      <Form.Label className="required">Video Platform</Form.Label>
                      <Form.Control
                        as="select"
                        name="video_platform"
                        defaultValue=""
                        ref={register({
                          required: {
                            value: true,
                            message: "Type Required!",
                          },
                        })}
                      >
                        <option value="" disabled>
                          Select Video Platform
                        </option>
                      </Form.Control>
                      {errors.video_platform && (
                        <p className="text-danger">
                          {errors.video_platform.message}
                        </p>
                      )}
                    </Form.Group>
                    <Form.Group controlId="exampleForm.ControlInput2">
                      <Form.Label>
                        Description
                      </Form.Label>
                      <Form.Control
                        as="textarea"
                        rows="3"
                        name="description"
                        placeholder="Description"
                        ref={register()}
                      />
                    </Form.Group>
                </Col>
              </Row>
                <Button variant="primary" type="submit">
                  Add
                </Button>{" "}
                <Button variant="outline-secondary" type="reset">
                  Clear
                </Button>
              </Form>
              <br />
              <Row>
                <Col md={3} />
                <Col md={6}>
                  {showMsg ? (
                    <Alert
                      variant="success"
                      onClose={() => setShowMsg(false)}
                      dismissible
                    >
                      Video added successfully!
                    </Alert>
                  ) : (
                    ""
                  )}

                  {showErr ? (
                    <Alert
                      variant="danger"
                      onClose={() => setShowErr(false)}
                      dismissible
                    >
                      {errorMessage}
                    </Alert>
                  ) : (
                    ""
                  )}
                </Col>
              </Row>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Aux>
  );
};

export default AddVideo;
