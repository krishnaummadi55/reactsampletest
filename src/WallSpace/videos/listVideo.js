   import React, { useEffect, useState } from "react";
   import { Row, Col, Card, Table, Button, Form, Badge } from "react-bootstrap";
   import Aux from "../../hoc/_Aux";
   import Breadcrumb from "../../App/components/Breadcrumb";
   import Paginator from "react-hooks-paginator";
   import { Link } from "react-router-dom";
   
   const listVideo = (props) => {

     const handleSubmit = (e) => {
     };
     return (
       <Aux>
         <Row className="align-items-center page-header">
           <Col>
             <Breadcrumb />
           </Col>
         </Row>
         <Row>
           <Col>
             <Card>
               <Card.Header as="h3">
                 Videos Details{"    "}
                 <Link to="/videos/addVideo">
                   <Button varient="primary">New</Button>
                 </Link>
               </Card.Header>
               <Card.Body>
                 <Row>
                   <Col md={6}>
                     <Card.Title className="mb-2 text-muted" as="h5">
                       List of Videos
                     </Card.Title>
                   </Col>
                   <Col md={3}></Col>
                   <Col md={3}>
                     <Form.Group controlId="exampleForm.ControlInput2">
                       <Form.Control
                         type="text"
                         placeholder="Search Videos..."
                         onChange={handleSubmit}
                       />
                     </Form.Group>
                   </Col>
                 </Row>
                 <Table responsive hover>
                   <thead>
                     <tr>
                       <th>#</th>
                       <th>Genre Name</th>
                       <th>Edit</th>
                     </tr>
                   </thead>
                   {/* <tbody>
                     {currentData.map((company, index) => (
                       <tr key={company.id ? company.id : index}>
                         <th scope="row">{index + 1}</th>
                         <td>{company.company_name}</td> 
                         <td>
                           <Button
                             variant="secondary"
                             size="sm"
                             onClick={() => editCustomer(company.id)}
                           >
                             Edit
                           </Button>
                         </td>
                       </tr>
                     ))}
                   </tbody>*/}
                 </Table> 
               </Card.Body>
             </Card>
           </Col>
         </Row>
       </Aux>
     );
   };
   export default listVideo;
   