import React from "react";
import { usePromiseTracker } from "react-promise-tracker";
import Loader from "react-loader-spinner";

const LoadingIndicator = (props) => {
  const { promiseInProgress } = usePromiseTracker();

  return (
    promiseInProgress && (
      <div
        style={{
          width: "100%",
          height: "100",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Loader type="Bars" color="#12a0b1" height="100" width="100" />
      </div>
    )
  );
};

export default LoadingIndicator;

{
  /* 

import { trackPromise } from "react-promise-tracker";
// <LoadingIndicator />
import LoadingIndicator from "../wsTools";

  //usage: 

  <LoadingIndicator /> 
  
  */
}
