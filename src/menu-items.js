export default {
    items: [
        {
            id: "dashboard",
            type: "group",
            children: [
                {
                    id: "server",
                    title: "Dashboard",
                    type: "item",
                    url: "/dashboard/maindashboard",
                    icon: "feather icon-activity"
                },
                {
                    id: "listcustomer",
                    title: "Genres",
                    icon: "feather icon-users",
                    url: "/genres/listGenres",
                    type: "item"
                },
                {
                    id: "listvideo",
                    title: "Videos",
                    icon: "feather icon-users",
                    url: "/videos/listVideo",
                    type: "item"
                }
            ]
        },
        // {
        //     id: "customers",
        //     type: "group",
        //     children: [
        //         {
        //             id: "listcustomer",
        //             title: "Customers",
        //             icon: "feather icon-users",
        //             url: "/customers/list",
        //             type: "item"
        //         }
        //     ]
        // }
    ]
}