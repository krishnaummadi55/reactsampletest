import React from "react";
import $ from "jquery";

window.jQuery = $;
window.$ = $;
global.jQuery = $;

const MainDashboard = React.lazy(() =>import("./WallSpace/Dashboard/MainDashboard"));
const ListGenres = React.lazy(() =>import("./WallSpace/genres/listGenres"));
const AddGenres = React.lazy(() =>import("./WallSpace/genres/addGenres"));

const ListVideos = React.lazy(() =>import("./WallSpace/videos/listVideo"));
const AddVideos = React.lazy(() =>import("./WallSpace/videos/addVideo"));

const routes = [
  {
    path: "/dashboard/maindashboard",
    exact: true,
    name: "Crypto",
    component: MainDashboard,
  },
  {
    path: "/genres/listGenres",
    exact: true,
    name: "Crypto",
    component: ListGenres,
  },
  {
    path: "/genres/addGenres",
    exact: true,
    name: "Crypto",
    component: AddGenres,
  },
  {
    path: "/videos/listVideo",
    exact: true,
    name: "Crypto",
    component: ListVideos,
  },
  {
    path: "/videos/addVideo",
    exact: true,
    name: "Crypto",
    component: AddVideos,
  }
];

export default routes;
